package hw1;

import hw1.impl.Item;
import hw1.impl.Shop;
import java.util.Random;

/**
 *
 * @author Ladislav Mejzlík <lmejzlik@gmail.com>
 */
public class HW1 {

    private static final int GROUPS = 3;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        OneDollarShop shop = new Shop();
        Random r = new Random();
       
        shop.addItem(r.nextInt(GROUPS), new Item("AAA121", "Bread", ItemType.FOOD));
        shop.addItem(r.nextInt(GROUPS), new Item("BAA111", "Milk", ItemType.FOOD));
        shop.addItem(r.nextInt(GROUPS), new Item("AAB121", "Cheese", ItemType.FOOD));
        shop.addItem(r.nextInt(GROUPS), new Item("AAA141", "Ham", ItemType.FOOD));
        
        shop.addItem(r.nextInt(GROUPS), new Item("ABA111", "Notebook", ItemType.ELECTRONIC));
        shop.addItem(r.nextInt(GROUPS), new Item("AAB121", "Phone", ItemType.ELECTRONIC));
        shop.addItem(r.nextInt(GROUPS), new Item("BAA111", "Camera", ItemType.ELECTRONIC));
        shop.addItem(r.nextInt(GROUPS), new Item("BAA151", "Keyboard", ItemType.ELECTRONIC));

        System.out.println("Everything:");
        for (int i = 0; i < GROUPS; i++) {
            System.out.println(shop.getSortedItemsForCertainPrice(i));
        }
        
        System.out.println("\nFood:");
        for (int i = 0; i < GROUPS; i++) {
            System.out.println(shop.getSortedFoodsForPrice(i));
        }
        
        System.out.println("\nElectronics:");
        for (int i = 0; i < GROUPS; i++) {
            System.out.println(shop.getSortedElectronicsForPrice(i));
        }
    }

}
