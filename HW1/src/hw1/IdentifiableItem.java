package hw1;

public interface IdentifiableItem extends Comparable<IdentifiableItem>{

    public String getId();

    public String getName();

    public ItemType getItemType();
}
