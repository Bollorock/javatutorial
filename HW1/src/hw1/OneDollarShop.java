package hw1;

import java.util.SortedSet;

public interface OneDollarShop {

    public int addItem(int price, IdentifiableItem item);

    public SortedSet<IdentifiableItem> getSortedItemsForCertainPrice(int price);

    public SortedSet<IdentifiableItem> getSortedElectronicsForPrice(int price);

    public SortedSet<IdentifiableItem> getSortedFoodsForPrice(int price);

}
