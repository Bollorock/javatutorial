package hw1.impl;

import hw1.IdentifiableItem;
import hw1.ItemType;
import hw1.OneDollarShop;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.SortedSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 *
 * @author Ladislav Mejzlík <lmejzlik@gmail.com>
 */
public class Shop implements OneDollarShop {

    private final Map<Integer, Set<IdentifiableItem>> items;

    public Shop() {
        items = new HashMap<>();
    }

    public SortedSet<IdentifiableItem> getSortedItems(int price, ItemType itemType) {
        Set<IdentifiableItem> itemsByPrice = items.get(price);
        if (itemsByPrice == null) {
            return getUnmodifiableSortedSet(null);
        }

        Set<IdentifiableItem> filtered = itemsByPrice.parallelStream().filter(
                (IdentifiableItem t) -> t.getItemType() == itemType
        ).collect(Collectors.toSet());

        return getUnmodifiableSortedSet(filtered);
    }

    @Override
    public int addItem(int price, IdentifiableItem item) {
        Set<IdentifiableItem> itemsByPrice = items.get(price);
        if (itemsByPrice == null) {
            itemsByPrice = new HashSet<>();
            items.put(price, itemsByPrice);
        }
        itemsByPrice.add(item);
        return items.size();
    }

    @Override
    public SortedSet<IdentifiableItem> getSortedItemsForCertainPrice(int price) {
        return getUnmodifiableSortedSet(items.get(price));
    }

    @Override
    public SortedSet<IdentifiableItem> getSortedElectronicsForPrice(int price) {
        return getSortedItems(price, ItemType.ELECTRONIC);
    }

    @Override
    public SortedSet<IdentifiableItem> getSortedFoodsForPrice(int price) {
        return getSortedItems(price, ItemType.FOOD);
    }

    private SortedSet<IdentifiableItem> getUnmodifiableSortedSet(Set<IdentifiableItem> items) {
        return items == null ? Collections.unmodifiableSortedSet(new TreeSet()) : Collections.unmodifiableSortedSet(new TreeSet(items));
    }

}
