package hw1.impl;

import hw1.IdentifiableItem;
import hw1.ItemType;
import java.util.Objects;

/**
 *
 * @author Ladislav Mejzlík <lmejzlik@gmail.com>
 */
public class Item implements IdentifiableItem {

    private final String id;
    private final String name;
    private final ItemType itemType;

    public Item(String id, String name, ItemType type) {
        this.id = id;
        this.name = name;
        this.itemType = type;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public ItemType getItemType() {
        return itemType;
    }

    @Override
    public int compareTo(IdentifiableItem other) {
        final int typeOrder = this.getItemType().compareTo(other.getItemType());
        if (typeOrder != 0) {
            return typeOrder;
        }

        final int nameOrder = this.getName().compareTo(other.getName());
        if (nameOrder != 0) {
            return nameOrder;
        }

        return this.getId().compareTo(other.getId());
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + Objects.hashCode(this.id);
        hash = 53 * hash + Objects.hashCode(this.itemType);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final Item other = (Item) obj;
        return this.itemType == other.itemType && Objects.equals(this.id, other.id);
    }

    @Override
    public String toString() {
        return "Item{" + "id=" + id + ", name=" + name + ", itemType=" + itemType + '}';
    }
}
