/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lesson4.notify;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ladislav Mejzlík <lmejzlik@gmail.com>
 */
public class Waiter implements Runnable {

    private final Message msg;

    public Waiter(Message msg) {
        this.msg = msg;
    }

    @Override
    public void run() {
        String name = Thread.currentThread().getName();
        synchronized (msg) {
            try {
                System.out.println(name + " waiting to be notified from: " + System.currentTimeMillis());
                msg.wait();
            } catch (InterruptedException ex) {
                Logger.getLogger(Waiter.class.getName()).log(Level.SEVERE, null, ex);
            }
            System.out.println(name + " notified at: " + System.currentTimeMillis());
            System.out.println(name + " processed msg: " + msg.getMsg());
        }
    }

}
