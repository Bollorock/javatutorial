/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lesson4.notify;

/**
 *
 * @author Ladislav Mejzlík <lmejzlik@gmail.com>
 */
public class Test {
    public static void main(String[] args) {
        Message msg = new Message("Procces it");
        
        Waiter waiter0 = new Waiter(msg);
        new Thread(waiter0, "waiter0").start();
        
        Waiter waiter1 = new Waiter(msg);
        new Thread(waiter1, "waiter1").start();
        
        Waiter waiter2 = new Waiter(msg);
        new Thread(waiter2, "waiter2").start();
        
        Waiter waiter3 = new Waiter(msg);
        new Thread(waiter3, "waiter3").start();
        
        Waiter waiter4 = new Waiter(msg);
        new Thread(waiter4, "waiter4").start();
        
        Waiter waiter5 = new Waiter(msg);
        new Thread(waiter5, "waiter5").start();
        
        Notifier notifier = new Notifier(msg);
        new Thread(notifier, "notifier").start();
        
        System.out.println("All threads started");
        
    }
}
