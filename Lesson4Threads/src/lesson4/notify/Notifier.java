/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lesson4.notify;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ladislav Mejzlík <lmejzlik@gmail.com>
 */
public class Notifier implements Runnable {

    private final Message msg;

    public Notifier(Message msg) {
        this.msg = msg;
    }

    @Override
    public void run() {
        String name = Thread.currentThread().getName();
        System.out.println(name + ": started");
        try {
            Thread.sleep(1000);
            synchronized (msg) {
                msg.setMsg(name + ": Notifier work done");
//                msg.notify();
                msg.notifyAll();
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(Notifier.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
