package lesson4;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ladislav Mejzlík <lmejzlik@gmail.com>
 */
public class TestConcurency {

    public static void main(String[] args) {
        final StringBuilder sb = new StringBuilder("0 1 2 3 4 5 6 7 8 9");
        System.out.println(sb);

        Thread t1 = new Thread(() -> {
            for (int i = 0; i < 1000; i++) {
                sb.reverse();
            }
        });

        Thread t2 = new Thread(() -> {
            for (int i = 0; i < 1000; i++) {
                sb.reverse();
            }
        });

        t1.start();
        t2.start();

        try {
            t1.join();
            t2.join();
        } catch (InterruptedException ex) {
            Logger.getLogger(TestConcurency.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println(sb);
    }

}
