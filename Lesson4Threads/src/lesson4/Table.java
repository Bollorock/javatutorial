/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lesson4;

/**
 *
 * @author Ladislav Mejzlík <lmejzlik@gmail.com>
 */
class Table {

    synchronized void printTable(int n) {
        //synchronized method         
        for (int i = n; i >= 1; i--) {
            System.out.println(i);
            try {
                Thread.sleep(400);
            } catch (Exception e) {
                System.out.println(e);
            }
        }
        System.out.println("Thread exiting.");
    }

    synchronized void printTable2(int n) {
        //synchronized method         
        for (int i = n; i >= 1; i--) {
            System.out.println(i);
            try {
                Thread.sleep(400);
            } catch (Exception e) {
                System.out.println(e);
            }
        }
        System.out.println("Thread exiting.");
    }
}





