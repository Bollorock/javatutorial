package lesson4;

/**
 *
 * @author Ladislav Mejzlík <lmejzlik@gmail.com>
 */
public class Lesson4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        MyThread myThread = new MyThread();
        myThread.start();

        new Thread(new MyRunnable()).start();

        new Thread(() -> {
            System.out.println("Hello Lambda");
        }).start();

        System.out.println(Thread.currentThread().getName());
    }

}
