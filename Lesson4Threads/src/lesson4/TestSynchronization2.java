/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lesson4;

/**
 *
 * @author Ladislav Mejzlík <lmejzlik@gmail.com>
 */
public class TestSynchronization2 {

    /**
     * *@paramargs
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        Table obj = new Table();
        //only one object     
        MyThread1 t1 = new MyThread1(obj);
        MyThread2 t2 = new MyThread2(obj);
        t1.start();
        t2.start();
    }
}
