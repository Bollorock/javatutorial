/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hw2;

import java.time.LocalDateTime;

/**
 *
 * @author Ladislav Mejzlík <lmejzlik@gmail.com>
 */
public interface WhatsappMessage {

    public int getIndex();

    public void setIndex(int index);

    public String getFrom();

    public String getContent();

    public LocalDateTime getTimestamp();
}
