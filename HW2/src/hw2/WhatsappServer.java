/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hw2;

import java.util.List;

/**
 *
 * @author Ladislav Mejzlík <lmejzlik@gmail.com>
 */
//There should be one server in one thread
public interface WhatsappServer extends Runnable {

    public int writeMessage(WhatsappMessage message);

    public List<WhatsappMessage> readMessage(int index);
}
