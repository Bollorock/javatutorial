/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hw2.impl;

import hw2.WhatsappMessage;
import java.time.LocalDateTime;

/**
 *
 * @author Ladislav Mejzlík <lmejzlik@gmail.com>
 */
public class Message implements WhatsappMessage {

    private int index;
    private final String from;
    private final String content;
    private final LocalDateTime timestamp;

    public Message(String from, String content) {
        this.from = from;
        this.content = content;
        this.timestamp = LocalDateTime.now();
    }

    @Override
    public int getIndex() {
        return index;
    }

    @Override
    public void setIndex(int index) {
        this.index = index;
    }

    @Override
    public String getFrom() {
        return from;
    }

    @Override
    public String getContent() {
        return content;
    }

    @Override
    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    @Override
    public String toString() {
        return from + "\t>> " + content;
    }

}
