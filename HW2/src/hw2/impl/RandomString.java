/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hw2.impl;

import java.util.Random;

/**
 *
 * @author Ladislav Mejzlík <lmejzlik@gmail.com>
 */
public class RandomString {

    private static final Random r = new Random();
    private static final String[] lines = new String[]{
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
        "Sed ultrices ex et neque ullamcorper convallis.",
        "Quisque vitae risus quis dolor pulvinar egestas.",
        "Pellentesque euismod purus vel justo congue ullamcorper.",
        "Sed varius erat vitae magna mattis, at accumsan nulla rhoncus.",
        "Etiam consectetur mauris vitae vulputate maximus.",
        "Donec ultrices ex fermentum tellus ullamcorper pulvinar.",
        "Vestibulum vel sem at massa dictum porta sed sit amet lorem.",
        "Curabitur vulputate augue id orci consequat, consequat ultricies metus consectetur.",
        "Donec aliquet velit vitae nibh efficitur ornare.",
        "Donec facilisis nisl ac orci consequat, sit amet sodales lectus tempus.",
        "Cras vel arcu viverra, rutrum tortor et, congue ipsum.",
        "Vestibulum suscipit dui eu tortor dictum elementum.",
        "Pellentesque pellentesque felis eu commodo vulputate.",
        "Praesent eleifend lectus et leo condimentum congue.",
        "Proin imperdiet nisl et pulvinar pharetra.",
        "Curabitur laoreet justo id erat malesuada, vel posuere dolor semper.",
        "Curabitur ultricies augue non semper fringilla.",
        "Maecenas nec elit at nulla egestas mattis sit amet ac ligula.",
        "Pellentesque rutrum purus sit amet sem porttitor, commodo fermentum turpis dignissim.",
        "Mauris quis nunc pretium, interdum eros a, aliquet dui.",
        "Cras a lacus sed velit commodo elementum eget eget leo."
    };

    public static String getRandomString() {
        return lines[r.nextInt(lines.length)];
    }
}
