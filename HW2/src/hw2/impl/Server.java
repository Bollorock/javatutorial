package hw2.impl;

import hw2.WhatsappMessage;
import hw2.WhatsappServer;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Ladislav Mejzlík <lmejzlik@gmail.com>
 */
public class Server implements WhatsappServer {

    private static Server instance;

    private final List<WhatsappMessage> messages;

    public static Server getServer() {
        if (instance == null) {
            synchronized (Server.class) {
                if (instance == null) {
                    instance = new Server();
                }
            }
        }
        return instance;
    }

    private Server() {
        this.messages = new ArrayList<>();
    }

    @Override
    public void run() {
    }

    @Override
    synchronized public int writeMessage(WhatsappMessage message) {
        message.setIndex(messages.size());
        messages.add(message);
        System.out.println(message);
        return message.getIndex();
    }

    @Override
    synchronized public List<WhatsappMessage> readMessage(int index) {
        ArrayList<WhatsappMessage> result = new ArrayList<>();
        for (int i = index; i < messages.size(); i++) {
            result.add(messages.get(i));
        }
        return result;
    }

}
