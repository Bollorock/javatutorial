/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hw2.impl;

import hw2.WhatsappClient;
import hw2.WhatsappMessage;
import hw2.WhatsappServer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 *
 * @author Ladislav Mejzlík <lmejzlik@gmail.com>
 */
public class Client implements WhatsappClient {

    private final String name;
    private final List<WhatsappMessage> messages;
    private final WhatsappServer server;
    private final Timer timer;
    private int lastMessage;

    public Client(String name) {
        this.messages = new ArrayList<>();
        this.server = Server.getServer();
        this.timer = new Timer();
        lastMessage = 0;
        this.name = name;
    }

    private void sendMessage(String content) {
        server.writeMessage(new Message(name, content));
    }

    @Override
    public List<WhatsappMessage> getAllMessages() {
        return Collections.unmodifiableList(messages);
    }

    @Override
    public void run() {
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                sendMessage(RandomString.getRandomString());
            }
        }, 0, 3000);

        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                readMessages();
            }
        }, 0, 3000);
    }

    private void readMessages() {
        List<WhatsappMessage> read = server.readMessage(lastMessage);
        if (read != null && !read.isEmpty()) {
            messages.addAll(read);
            lastMessage = read.get(read.size() - 1).getIndex();
        }
    }

    public void stop() {
        timer.cancel();
    }

}
