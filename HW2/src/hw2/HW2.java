/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hw2;

import hw2.impl.Client;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ladislav Mejzlík <lmejzlik@gmail.com>
 */
public class HW2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Client[] clients = new Client[]{
            new Client("George"),
            new Client("Charles"),
            new Client("John"),
            new Client("Clair"),
            new Client("Wilson")
        };

        for (Client client : clients) {
            new Thread(client).start();
        }

        try {
            Thread.sleep(20000);
        } catch (InterruptedException ex) {
            Logger.getLogger(HW2.class.getName()).log(Level.SEVERE, null, ex);
        }

        for (Client client : clients) {
            client.stop();
        }

        List<WhatsappMessage> allMessages = clients[0].getAllMessages();
        boolean ok = true;
        for (Client client : clients) {
            ok |= allMessages.equals(client.getAllMessages());
        }
        System.out.println("Passed: " + ok);
    }

}
